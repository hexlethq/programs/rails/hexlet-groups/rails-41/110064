# frozen_string_literal: true

# BEGIN
def get_same_parity(nums)
  first = nums.first
  nums.select { |i| i.odd? == first.odd? }
end
# END
