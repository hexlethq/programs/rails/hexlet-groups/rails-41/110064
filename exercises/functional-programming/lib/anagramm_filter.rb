# frozen_string_literal: true

# BEGIN
def anagramm_filter(word, words)
  sort_word = word.split('').sort.join('')
  words.inject([]) { |acc, item| item.split('').sort.join('') == sort_word ? acc << item : acc }
end
# END
