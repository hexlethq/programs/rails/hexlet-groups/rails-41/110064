# frozen_string_literal: true

# BEGIN
def count_by_years(users)
  users
    .select { |u| u[:gender] == 'male' }
    .map { |u| u[:birthday].split('-').first }
    .each_with_object({}) do |year, acc|
      acc[year] ||= 0
      acc[year] += 1
    end
end
# END
