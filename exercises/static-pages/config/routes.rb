# frozen_string_literal: true

Rails.application.routes.draw do
  root 'home#index'
  # get 'pages/show', to: 'pages#show'
  resources :pages, only: :show
end
