# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def test_instance_of_stack
    s = Stack.new
    assert_instance_of Stack, s
    assert(s.empty?)
    assert_equal(s.size, 0)
  end

  def test_pop_from_emply_stack
    s = Stack.new
    s.pop!
    assert(s.empty?)
    assert_equal(s.size, 0)
  end

  def test_pop_from_non_empty_stack
    s = Stack.new(%w[ruby javascript elixir])
    s.pop!
    assert(s.to_a == %w[ruby javascript])
  end

  def test_pop_get_first_element_stack
    s = Stack.new(%w[ruby javascript elixir])
    first_element = s.pop!
    assert(first_element == 'elixir')
  end

  def test_push_set_last_element_stack
    s = Stack.new(%w[ruby javascript])
    s.push! 'elixir'
    assert_equal(s.to_a.last, 'elixir')
  end

  def test_push_stack
    s = Stack.new
    s.push! 'ruby'
    assert(s.to_a == ['ruby'])
  end

  def test_empty_stack
    s = Stack.new
    assert_empty s
  end

  def test_size_stack
    s = Stack.new(%w[ruby javascript elixir])
    assert_equal s.size, 3
    s.pop!
    assert_equal s.size, 2
    s.push! 'haskell'
    assert_equal s.size, 3
  end

  def test_clear_stack
    s = Stack.new(%w[ruby javascript elixir])
    s.clear!
    assert_empty s
    assert(s.empty?)
    assert_equal(s.size, 0)
  end

  def test_stack_to_array
    s = Stack.new
    assert_instance_of Array, s.to_a
    s = Stack.new(%w[ruby javascript elixir])
    assert_instance_of Array, s.to_a
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
