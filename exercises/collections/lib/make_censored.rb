# frozen_string_literal: true

# rubocop:disable Style/For

def make_censored(text, stop_words)
  # BEGIN
  censored_text = []
  all_words = text.split ' '
  all_words.each do |word|
    include_word = stop_words.any? { |stop_word| stop_word == word }
    if (include_word)
      censored_text << '$#%!'
    else
      censored_text << word
    end
  end
  censored_text.join ' '
  # END
end
# rubocop:enable Style/For
