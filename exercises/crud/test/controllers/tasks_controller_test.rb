require "test_helper"

class TasksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @attrs = { name: "Task new",  description: "Description 2", creator: "Creator 2" }
  end

  test "get all tasks" do
    get root_path
    assert_response :success
  end

  test "get one task" do
    get task_path(tasks(:task_1))
    assert_response :success
    assert_select 'h1', 'Task #1: Task name 1'
  end

  test "open new task page" do
    get new_task_path
    assert_response :success
    assert_select 'h1', "New task"
  end

  test "should create task" do
    post tasks_path, params: { task: @attrs }
    task = Task.find_by! name: @attrs[:name]
    assert_redirected_to task_path(task)
  end

  test "open update task page" do
    get edit_task_path(tasks(:task_2))
    assert_response :success
    assert_select 'h1', "Edit task"
  end

  test "update task" do
    patch task_path(tasks(:task_2)), params: { task: { name: "Task rename" } }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_select 'h1', 'Task #463292: Task rename'
  end

  test "destroy task" do
    task = tasks(:task_1)
    assert_difference("Task.count", -1) do
      delete task_path(task)
    end
    assert_redirected_to root_path
  end
end
