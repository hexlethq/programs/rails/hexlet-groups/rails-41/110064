class Task < ApplicationRecord
  validates :name, :description, :creator, presence: true
  validates :name, :description, :creator, format: {
    with: /\A[a-zA-Z0-9 ]+\z/,
    message: "only allows letters"
  }
end
