module Posts
  class CommentsController < ApplicationController
    before_action :set_post, only: %i[index new create]
    before_action :set_comment, only: %i[show edit update destroy]

    # GET /comments or /comments.json
    def index
      @comments = @post.comments
    end

    # GET /comments/1 or /comments/1.json
    def show
    end

    # GET /comments/new
    def new
      @comment = @post.comments.build
    end

    # GET /comments/1/edit
    def edit
    end

    # POST /comments or /comments.json
    def create
      @comment = @post.comments.build(comment_params)

      if @comment.save
        redirect_to comment_path(@comment), notice: "Comment was successfully created."
      else
        render :new, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /comments/1 or /comments/1.json
    def update
      if @comment.update(comment_params)
        redirect_to comment_path(@comment), notice: "Comment was successfully updated."
      else
        render :edit, status: :unprocessable_entity
      end
    end

    # DELETE /comments/1 or /comments/1.json
    def destroy
      @comment.destroy

      redirect_to post_path(@comment.post), notice: "Comment was successfully destroyed."
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_post
        @post = Post.find(params[:post_id])
      end

      def set_comment
        @comment = Comment.find(params[:id])
      end

      # Only allow a list of trusted parameters through.
      def comment_params
        params.require(:comment).permit(:body, :post_id)
      end
  end
end
