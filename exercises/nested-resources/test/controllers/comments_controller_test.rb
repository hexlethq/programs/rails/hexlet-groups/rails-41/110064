require "test_helper"

class CommentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @comment = comments(:one)
  end

  test "should get index" do
    get post_comments_url(@comment.post_id)
    assert_response :success
  end

  test "should get new" do
    get new_post_comment_url(@comment.post_id)
    assert_response :success
  end

  test "should create comment" do
    post post_comments_url(@comment.post_id), params: { comment: { post_id: @comment.post_id, body: @comment.body } }
    # вот это почему-то не работает, разные id у комментов
    # comment = Comment.find_by! body: @comment.body
    # assert_redirected_to comment_url(comment)
    follow_redirect!
    assert_response :success
    assert_select "p", @comment.body
  end

  test "should show comment" do
    get comment_url(@comment)
    assert_response :success
  end

  test "should get edit" do
    get edit_comment_url(@comment)
    assert_response :success
  end

  test "should update comment" do
    patch comment_url(@comment), params: { comment: { post_id: @comment.post_id, body: 'www' } }
    assert_redirected_to comment_url(@comment)
  end

  test "should destroy comment" do
    assert_difference('Comment.count', -1) do
      delete comment_url(@comment)
    end

    assert_redirected_to post_url(@comment.post_id)
  end
end
