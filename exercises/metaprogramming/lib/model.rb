# frozen_string_literal: true

# BEGIN
module Model
  def initialize(attrs = {})
    @params = {}
    self.class.schema.each do |name, options|
      @params[name] = attrs.key?(name) ? transform_by_type(attrs[name], options[:type]) : nil
    end
  end

  def attributes
    @params
  end

  def transform_by_type(value, type)
    return value if value.nil?

    case type
    when :datetime
      DateTime.parse value
    when :integer
      value.to_i
    when :string
      value.to_s
    when :boolean
      !!value
    end
  end

  module ClassMethods
    attr_accessor :params
    attr_reader :schema

    def attribute(name, options = {})
      @schema ||= {}
      @schema[name] = options
      define_method name do
        @params[name]
      end
      define_method "#{name}=" do |value|
        @params[name] = transform_by_type(value, options[:type])
      end
    end
  end

  def self.included(base)
    base.extend(ClassMethods)
  end
end
# END
