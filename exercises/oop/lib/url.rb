# frozen_string_literal: true

require 'uri'
require 'forwardable'

# BEGIN
class Url
  attr_accessor :address
  attr_reader :query_params

  extend URI
  extend Forwardable
  include Comparable

  def initialize(address)
    @address = URI(address)
    @query_params = create_query_params
  end

  def <=>(other)
    address <=> other.address
  end

  def_delegators :address, :scheme, :host

  def query_param(key, default_value = nil)
    query_params[key] || default_value
  end

  private

  def create_query_params
    return nil if address.query.nil?

    address
      .query
      .split('&')
      .each_with_object({}) do |pair, acc|
        key, value = pair.split('=')
        acc[key.to_sym] = value
      end
  end
end
# END
