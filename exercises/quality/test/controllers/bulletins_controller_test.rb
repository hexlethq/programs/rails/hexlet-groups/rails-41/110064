require "test_helper"

class BulletinsControllerTest < ActionDispatch::IntegrationTest
  test "open all bulletins page" do
    get '/'
    assert_response :success
    assert_select 'h1', 'Bulletins'
  end

  test "open one bulletin page" do
    get bulletin_path(bulletins(:bulletin_1))
    # debugger
    assert_response :success
    assert_select 'h1', 'Bulletin_1'
  end
end
