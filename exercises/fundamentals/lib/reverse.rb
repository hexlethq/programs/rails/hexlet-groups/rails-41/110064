# frozen_string_literal: true

# BEGIN
def reverse(str)
  str
    .split('')
    .each_with_object([]) do |letter, acc|
      acc.unshift letter
    end
    .join
end
# END
