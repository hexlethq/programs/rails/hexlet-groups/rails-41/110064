# frozen_string_literal: true

# BEGIN
def fibonacci(index)
  return nil if index.negative?

  fib = [0, 1]
  while fib.size <= index
    fib << fib[-2] + fib[-1]
  end
  fib[index - 1]
end
# END
