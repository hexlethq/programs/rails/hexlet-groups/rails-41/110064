# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  return '' if start > stop

  # range = [*start...stop]
  range = start != stop ? [*start..stop] : [start]
  range.each_with_object([]) do |item, acc|
    if (item % 3).zero? && (item % 5).zero?
      acc << 'FizzBuzz'
    elsif (item % 5).zero?
      acc << 'Buzz'
    elsif (item % 3).zero?
      acc << 'Fizz'
    else
      acc << item
    end
  end.join ' '
end
# END
